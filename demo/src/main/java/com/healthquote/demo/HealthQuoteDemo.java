package com.healthquote.demo;


import java.util.Scanner;

import com.healthcare.impl.HealthQuoteCalculatorImpl;
import com.healthcare.model.PersonDetails;

public class HealthQuoteDemo {

	public static void main(String[] args) {
		PersonDetails personDetails = new PersonDetails();
		HealthQuoteCalculatorImpl quoteCalculator = new HealthQuoteCalculatorImpl();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to Healthquote");
		System.out.println("Please enter your details for receiving a quote");
		System.out.println("Name:");
		personDetails.setName(scanner.nextLine());
		System.out.println("Gender (Male/Female):");
		personDetails.setGender(scanner.nextLine());
		System.out.println("Age:");
		personDetails.setAge(scanner.nextInt());
		scanner.nextLine();
		
		System.out.println("Current health");
		System.out.println("Hypertension(Yes/No):");
		personDetails.setHypertension(scanner.nextLine().equalsIgnoreCase("Yes")? true: false);		
		System.out.println("Boold Pressure(Yes/No):");
		personDetails.setBloodPressure(scanner.nextLine().equalsIgnoreCase("Yes")? true: false);
		System.out.println("Boold Suger(Yes/No):");
		personDetails.setBloodSuger(scanner.nextLine().equalsIgnoreCase("Yes")? true: false);
		System.out.println("Overweight(Yes/No):");
		personDetails.setOverWeight(scanner.nextLine().equalsIgnoreCase("Yes")? true: false);

		System.out.println("Habits");
		System.out.println("Smoking(Yes/No):");
		personDetails.setSmoking(scanner.nextLine().equalsIgnoreCase("Yes")? true: false);		
		System.out.println("Alcohol(Yes/No):");
		personDetails.setAlcohol(scanner.nextLine().equalsIgnoreCase("Yes")? true: false);
		System.out.println("Daily exercise(Yes/No):");
		personDetails.setDailyExercise(scanner.nextLine().equalsIgnoreCase("Yes")? true: false);
		System.out.println("Drugs(Yes/No):");
		personDetails.setDrugs(scanner.nextLine().equalsIgnoreCase("Yes")? true: false);
		
		int premium = quoteCalculator.getHealthQuote(personDetails);
		
		System.out.println("Health Insurance Premium for Mr." + personDetails.getName() + " is Rs. " + premium);

	}

}
