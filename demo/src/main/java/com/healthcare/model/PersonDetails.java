package com.healthcare.model;

public class PersonDetails {
	private String name;	
	private String gender;
	private int age;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	private Boolean hypertension;
	private Boolean bloodPressure;
	private Boolean bloodSuger;
	private Boolean overWeight;
	private Boolean smoking;
	private Boolean alcohol;
	private Boolean dailyExercise;
	private Boolean drugs;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Boolean getHypertension() {
		return hypertension;
	}
	public void setHypertension(Boolean hypertension) {
		this.hypertension = hypertension;
	}
	public Boolean getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(Boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public Boolean getBloodSuger() {
		return bloodSuger;
	}
	public void setBloodSuger(Boolean bloodSuger) {
		this.bloodSuger = bloodSuger;
	}
	public Boolean getOverWeight() {
		return overWeight;
	}
	public void setOverWeight(Boolean overWeight) {
		this.overWeight = overWeight;
	}
	public Boolean getSmoking() {
		return smoking;
	}
	public void setSmoking(Boolean smoking) {
		this.smoking = smoking;
	}
	public Boolean getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(Boolean alcohol) {
		this.alcohol = alcohol;
	}
	public Boolean getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(Boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public Boolean getDrugs() {
		return drugs;
	}
	public void setDrugs(Boolean drugs) {
		this.drugs = drugs;
	}

}
