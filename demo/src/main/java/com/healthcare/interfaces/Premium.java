package com.healthcare.interfaces;

import com.healthcare.model.PersonDetails;

public interface Premium {
	
	public int getPremium(int premium, PersonDetails personDetails);

}
