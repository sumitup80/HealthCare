package com.healthcare.interfaces;

import com.healthcare.model.PersonDetails;

public interface HealthQuoteCalculator {
	
	public int getHealthQuote(PersonDetails personDetails);

}
