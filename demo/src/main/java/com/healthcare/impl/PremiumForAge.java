package com.healthcare.impl;

import com.healthcare.interfaces.Premium;
import com.healthcare.model.PersonDetails;

public class PremiumForAge implements Premium {

	public int getPremium(int premium, PersonDetails personDetails) {
		int premiumForAge = premium == 0 ? 5000 : premium;
		int age = personDetails.getAge();
		if(age > 0 && age < 18)
			premiumForAge =  5000;
		else if(age>=18 && age < 25) {
			premiumForAge = premiumForAge + ((premiumForAge * 10)/100);
		}
		else if(age>=25 && age < 30) {
			premiumForAge = premiumForAge + ((premiumForAge * 20)/100);
		}
		else if(age>=30 && age < 35) {
			premiumForAge = premiumForAge + ((premiumForAge * 30)/100);
		}
		else if(age>=35 && age < 40) {
			premiumForAge = premiumForAge + ((premiumForAge * 40)/100);
		}
		else if(age>=40) {
			premiumForAge = premiumForAge + ((premiumForAge * (40 + ((age-40)/5 * 20)))/100);
		}
		return premiumForAge;
	}

}
