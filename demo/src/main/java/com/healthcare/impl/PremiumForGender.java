package com.healthcare.impl;

import com.healthcare.interfaces.Premium;
import com.healthcare.model.PersonDetails;

public class PremiumForGender implements Premium {

	public int getPremium(int premium, PersonDetails personDetails) {
		int result = premium;
		if(personDetails.getGender().equalsIgnoreCase("Male"))
			premium = premium + (premium * 2/100);
		return premium;
	}

}
