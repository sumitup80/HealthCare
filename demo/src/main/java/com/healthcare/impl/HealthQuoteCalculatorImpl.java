package com.healthcare.impl;

import java.util.List;

import com.healthcare.interfaces.HealthQuoteCalculator;
import com.healthcare.interfaces.Premium;
import com.healthcare.model.PersonDetails;

public class HealthQuoteCalculatorImpl implements HealthQuoteCalculator {
	
	PremiumRules rules = new PremiumRules();
	
	public int getHealthQuote(PersonDetails personDetails) {
		int premium = 0;
		List<Premium> premiumRuleList = rules.getPremiumRuleList();
		for(Premium rule:premiumRuleList){
			premium = rule.getPremium(premium, personDetails);
		}

		return premium;
	}
	
	

}
