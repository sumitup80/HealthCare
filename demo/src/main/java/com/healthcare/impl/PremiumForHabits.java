package com.healthcare.impl;

import com.healthcare.interfaces.Premium;
import com.healthcare.model.PersonDetails;

public class PremiumForHabits implements Premium {

	public int getPremium(int premium, PersonDetails personDetails) {
		int result = premium;
		int percentage = 0;
		if(personDetails.getSmoking()== true)
			percentage -= 3;
		if(personDetails.getAlcohol()== true)
			percentage -= 3;
		if(personDetails.getDrugs()== true)
			percentage -= 3;
		if(personDetails.getDailyExercise()== true)
			percentage += 3;
		
		if(percentage > 0)
			premium = result + (result * percentage/100);
		return premium;
	}

}
