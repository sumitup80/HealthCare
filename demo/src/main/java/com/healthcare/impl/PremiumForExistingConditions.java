package com.healthcare.impl;

import com.healthcare.interfaces.Premium;
import com.healthcare.model.PersonDetails;

public class PremiumForExistingConditions implements Premium {

	public int getPremium(int premium, PersonDetails personDetails) {
		int result = premium;
		int percentage = 0;
		if(personDetails.getHypertension()== true)
			percentage += 1;
		if(personDetails.getBloodPressure()== true)
			percentage += 1;
		if(personDetails.getBloodSuger()== true)
			percentage += 1;
		if(personDetails.getOverWeight()== true)
			percentage += 1;
		
		if(percentage > 0)
			premium = result + (result * percentage/100);
		
		return premium;
	}

}
