package com.healthcare.impl;

import java.util.ArrayList;
import java.util.List;

import com.healthcare.interfaces.Premium;

public class PremiumRules {
	
	private PremiumForAge premiumForAge = new PremiumForAge();
	private PremiumForGender premiumForGender = new PremiumForGender();
	private PremiumForExistingConditions premiumForExistingConditions = new PremiumForExistingConditions();
	private PremiumForHabits premiumForHabits = new PremiumForHabits();
	
	public List<Premium> premiumRuleList = new ArrayList<Premium>();
	
	public List<Premium> getPremiumRuleList() {
		premiumRuleList.add(premiumForAge);
		premiumRuleList.add(premiumForGender);
		premiumRuleList.add(premiumForExistingConditions);
		premiumRuleList.add(premiumForHabits);
		
		return premiumRuleList;
	}

}
