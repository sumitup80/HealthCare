package demo;

import com.healthcare.impl.HealthQuoteCalculatorImpl;
import com.healthcare.impl.PremiumForAge;
import com.healthcare.impl.PremiumForExistingConditions;
import com.healthcare.impl.PremiumForGender;
import com.healthcare.impl.PremiumForHabits;
import com.healthcare.model.PersonDetails;

import junit.framework.TestCase;

public class QuoteTest extends TestCase{
	
	PersonDetails personDetails = new PersonDetails();
	HealthQuoteCalculatorImpl quoteCalculator = new HealthQuoteCalculatorImpl();
	PremiumForAge premiumForAge = new PremiumForAge();
	PremiumForGender premiumForGender = new PremiumForGender();
	PremiumForExistingConditions premiumForExistingConditions = new PremiumForExistingConditions();
	PremiumForHabits premiumForHabits = new PremiumForHabits();
	
	protected void setUp() throws Exception {

		super.setUp();

		personDetails.setName("Gomes");
		personDetails.setGender("Male");
		personDetails.setAge(34);
		personDetails.setHypertension(false);		
		personDetails.setBloodPressure(false);
		personDetails.setBloodSuger(false);
		personDetails.setOverWeight(true);

		personDetails.setSmoking(false);		
		personDetails.setAlcohol(true);
		personDetails.setDailyExercise(true);
		personDetails.setDrugs(false);

		}
	
	public void testPremiumForAge() {
		int premium = premiumForAge.getPremium(5000, personDetails);
		assertEquals(6500, premium);
	}
	
	public void testPremiumForGender() {	
		int premium = premiumForGender.getPremium(5000, personDetails);
		assertEquals(5100, premium);
	}
	
	public void testPremiumForExistingConditions() {	
		int premium = premiumForExistingConditions.getPremium(5000, personDetails);
		assertEquals(5050, premium);
	}
	
	public void testPremiumForHabits() {	
		int premium = premiumForHabits.getPremium(5000, personDetails);
		assertEquals(5000, premium);
	}
	
	public void testQuoteForMale() {	
		int premium = quoteCalculator.getHealthQuote(personDetails);
		assertEquals(6696, premium);
	}
	
	public void testQuoteForFeMale() {	
		personDetails.setGender("Female");
		int premium = quoteCalculator.getHealthQuote(personDetails);
		assertEquals(6565, premium);
	}

}
